@echo off

setlocal

REM ......................setup variables......................
if [%1]==[] (
    SET ARCH=64
) else (
    SET ARCH=%1
)

if [%2]==[] (
    goto :usage
) else (
    SET PASS=%2
)

if ["%ARCH%"]==["64"] (
    SET BINARCH=x64
    SET PYPATH=C:\Python36-x64
)

REM ......................get latest version number......................
for /f "delims=" %%a in ('%PYPATH%\python.exe version.py') do @set APPVER=%%a

REM ......................cleanup previous build scraps......................
rd /s /q build
rd /s /q dist
if not exist "..\..\bin\" ( mkdir ..\..\bin\ ) else ( del /q ..\..\bin\*.* )

REM ......................run pyinstaller......................
"%PYPATH%\scripts\pyinstaller.exe" --clean datseeo.win%ARCH%.spec

if exist "dist\datseeo.exe" (
    REM ......................add metadata to built Windows binary......................
    .\verpatch.exe dist\datseeo.exe /va %APPVER%.0 /pv %APPVER%.0 /s desc "DatSeeO" /s name "DatSeeO" /s copyright "(c) 2019 JK" /s product "DatSeeO %BINARCH%" /s company "bettercallbots.com"

    REM ................sign frozen EXE with self-signed certificate..........
    SignTool.exe sign /f "..\certs\DatSeeO.pfx" /t http://timestamp.comodoca.com/authenticode /p %PASS% dist\datseeo.exe

    REM ......................call Inno Setup installer build script......................
    cd ..\InnoSetup
    "C:\Program Files (x86)\Inno Setup 5\iscc.exe" installer_%BINARCH%.iss

    REM ................sign final redistributable EXE with self-signed certificate..........
    SignTool.exe sign /f "..\certs\DatSeeO.pfx" /t http://timestamp.comodoca.com/authenticode /p %PASS% output\Datseeo-%APPVER%-setup-win%ARCH%.exe

    cd ..\pyinstaller
)

endlocal
