from datseeo.core.db import save_entry
from datseeo.core.exporter import write_csv
from datseeo.core.workers import SpiderWorker


class SeoPresenter(object):
    def __init__(self, view):
        self.view = view
        self.view.initialize()
        self.worker = None
        self.search_params = None

    def cancel_existing_job(self):
        if self.worker:
            self.update_status(f"Cancelling search ... It may take ~{self.search_params.sleep_delay} seconds")
            self.worker.signals.cancel.emit()

    def start_job(self, search_params):
        self.search_params = search_params
        self.cancel_existing_job()
        self.view.reset_table()

        self.worker = SpiderWorker(search_params.search_query, search_params.no_pages, search_params.sleep_delay)
        self.worker.signals.progress.connect(self.view.update_progress)
        self.worker.signals.result.connect(self.process_results)
        self.worker.signals.finished.connect(self.job_finished)
        self.update_status(
            f"Fetching {search_params.no_pages} pages for above query. Delay {search_params.sleep_delay}")
        self.view.job_started(self.worker)

    def process_results(self, results):
        for idx, entry in enumerate(results):
            save_entry(entry)
            self.view.entry_saved(entry)

    def update_status(self, status_message):
        self.view.update_status(status_message)

    def job_finished(self):
        self.worker = None
        self.update_status(f"Job completed. Display all search results")
        self.view.job_finished()

    def save_csv(self, file_name):
        if file_name:
            self.update_status(f"Saving {file_name}")
            write_csv(file_name)
