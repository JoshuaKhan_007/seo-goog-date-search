import requests


def get_results(query, page):
    try:
        response = requests.get(
            url="https://www.google.com/search",
            params={
                "q": query,
                "client": "firefox-b-ab",
                "start": page * 10,
                "source": "lnt",
                "tbs": "qdr:y",
            },
            headers={
                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:64.0) Gecko/20100101 Firefox/64.0",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                "Accept-Language": "en-GB,en;q=0.5",
                "Accept-Encoding": "gzip",
                "Upgrade-Insecure-Requests": "1",
                "Pragma": "no-cache",
                "Cache-Control": "no-cache"
            },
            timeout=5
        )
        return response.text
    except requests.exceptions.RequestException as e:
        print('HTTP Request failed')
        print(e)


def read_search_results(file_path):
    return file_path.read_text(encoding='utf-8')
