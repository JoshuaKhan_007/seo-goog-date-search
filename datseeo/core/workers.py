import time

from PyQt5.QtCore import *
from datseeo.core.goog_search import search


class SpiderSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(str)
    progress = pyqtSignal(dict)
    result = pyqtSignal(list)
    cancel = pyqtSignal()


class SpiderWorker(QRunnable):

    def __init__(self, search_query, no_pages, sleep_delay):
        super(SpiderWorker, self).__init__()
        self.signals = SpiderSignals()
        self.is_interrupted = False
        self.search_queries = search_query.split(';')
        self.no_pages = no_pages
        self.sleep_delay = sleep_delay
        self.signals.cancel.connect(self.cancel)

    def cancel(self):
        self.is_interrupted = True

    @pyqtSlot()
    def run(self):
        try:
            for sq in self.search_queries:
                query = sq.strip()
                for i in range(self.no_pages):
                    if self.is_interrupted:
                        break

                    self.signals.progress.emit({
                        "query": query,
                        "page": (i + 1)
                    })
                    time.sleep(self.sleep_delay)
                    results = search(query, i)
                    self.signals.result.emit(results)

        except Exception as e:
            self.signals.error.emit(str(e))

        self.signals.finished.emit()
