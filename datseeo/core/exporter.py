from datseeo.core.db import SearchEntry
from playhouse.csv_utils import dump_csv


def write_csv(file_path):
    fh = open(file_path, 'w', encoding='utf-8')
    dump_csv(SearchEntry.select(), fh, include_header=True, close_file=True)
