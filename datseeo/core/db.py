import tempfile
from datetime import datetime

from peewee import *

APP_DIR = tempfile.gettempdir()
SUFFIX = datetime.now().strftime('%Y%m%d%H%M%S')
DB_PATH = APP_DIR + f"/goog_dates_search_{SUFFIX}.db"
print(f"Saving database at {DB_PATH}")
db = SqliteDatabase(DB_PATH)


class SearchEntry(Model):
    link = CharField(primary_key=True)
    query = CharField()
    page = IntegerField()
    title = CharField()
    date = CharField()
    description = CharField()

    class Meta:
        database = db


db.create_tables([SearchEntry], safe=True)


def save_entry(search_entry):
    print(f"Saving search {search_entry.get('link')}")
    SearchEntry.insert(search_entry).on_conflict('ignore').execute()


def load_entries():
    return SearchEntry.select()
