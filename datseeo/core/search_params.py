from collections import namedtuple

SearchParams = namedtuple('SearchParams', 'search_query no_pages sleep_delay')
