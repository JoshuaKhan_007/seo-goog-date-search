import itertools

import click
from bs4 import BeautifulSoup
from datseeo.core.page_loader import get_results, read_search_results
from pathlib import Path


def format_date(date_text):
    return date_text.replace(" - ", "")


def get_result_description(search_query):
    date_element = search_query.find('span', {'class': 'f'})
    date = None
    if date_element:
        date = format_date(date_element.text)

    desc_element = search_query.find('span', {'class': 'st'})
    description = None
    if desc_element:
        description = desc_element.text
    return date, description


def get_result_header(search_query):
    link = search_query.find('a').attrs['href']
    title_element = search_query.find('h3')
    title = None
    if title_element:
        title = title_element.text
    return link, title


def soup_search_entry(query, page, search_entry):
    link, title = get_result_header(search_entry)
    date, description = get_result_description(search_entry)
    return {
        "query": query,
        "page": page,
        "link": link,
        "title": title,
        "date": date,
        "description": description
    }


def soup_results(soup):
    results_group = soup.find_all('div', {'class': 'srg'})
    return list(itertools.chain(*results_group))


def has_link(search_query):
    return True if search_query.find('a') else False


def parse_results(query, page, html_results):
    soup = BeautifulSoup(html_results, 'html.parser')
    search_entries = soup_results(soup)
    return [soup_search_entry(query, page, e) for e in search_entries if has_link(e)]


def write_html_to_disk(file_path, contents):
    file_path.write_text(contents, encoding='utf-8')
    print("Finished writing file to {0}".format(file_path))


def search(query, page):
    file_path = Path(f"search_results_{page}.html")
    print(f"File path: {file_path.absolute()}")
    if not file_path.exists():
        html_results = get_results(query, page)
        write_html_to_disk(file_path, html_results)
    html_results = read_search_results(file_path)
    return parse_results(query, page, html_results)


@click.command(short_help="Search for video")
@click.option("--query", required=True, type=str)
@click.option("--page", default=1, type=int)
def search_cli(query, page):
    click.echo(f"Searching for {query} and page {page}")
    results = search(query, page)
    for entry in results:
        click.echo(f"---{entry.get('link')}--")


if __name__ == '__main__':
    search_cli()
