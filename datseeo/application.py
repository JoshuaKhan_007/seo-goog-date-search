import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from datseeo.core.search_params import SearchParams
from datseeo.ui.base_window import Ui_MainWindow
from datseeo.ui.seo_presenter import SeoPresenter


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.presenter = SeoPresenter(self)

        self.results_model = QStandardItemModel(self)
        self.threadpool = QThreadPool()
        self.post_ui_setup()
        self.show()

    def initialize(self):
        self.setupUi(self)

    def post_ui_setup(self):
        self.results_view.setModel(self.results_model)
        self.reset_table()
        self.setup_event_handlers()
        self.setWindowTitle("SEO Results")

    def setup_event_handlers(self):
        self.fetch_btn.pressed.connect(self.start_job)
        self.export_btn.pressed.connect(self.save_csv)

    def save_csv(self):
        # @todo: check to see if we have data to save otherwise show a message
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getSaveFileName(
            self,
            "CSV file to save results",
            "",
            "All Files (*);;CSV Files (*.csv)",
            options=options
        )
        self.presenter.save_csv(file_name)

    def reset_table(self):
        self.results_model.clear()
        self.results_model.setHorizontalHeaderLabels([
            "Query",
            "Date",
            "Title",
            "Link",
            "Description"
        ])
        header = self.results_view.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QHeaderView.Stretch)

    def update_status(self, status_message):
        self.current_status_lbl.setText(status_message)

    def start_job(self):
        search_query = self.query_txt.text()
        no_pages = int(self.num_pages.text())
        sleep_delay = int(self.sleep_time.text())
        search_params = SearchParams(
            search_query=search_query,
            no_pages=no_pages,
            sleep_delay=sleep_delay
        )
        self.presenter.start_job(search_params)

    def job_started(self, worker):
        self.threadpool.start(worker)
        self.fetch_btn.pressed.disconnect(self.start_job)
        self.fetch_btn.setText("Cancel")
        self.fetch_btn.pressed.connect(self.cancel_existing_job)

    def job_finished(self):
        self.fetch_btn.pressed.disconnect(self.cancel_existing_job)
        self.fetch_btn.setText("Get Results")
        self.fetch_btn.pressed.connect(self.start_job)

    def cancel_existing_job(self):
        self.presenter.cancel_existing_job()

    def update_progress(self, p):
        self.update_status(f"Progress...Currently on page {p.get('page')} for {p.get('query')}")

    def entry_saved(self, entry):
        query = entry["query"]
        link = entry["link"]
        title = entry["title"]
        date = entry["date"]
        description = entry["description"]

        query_cell = QStandardItem(query)
        link_cell = QStandardItem(link)
        title_cell = QStandardItem(title)
        date_cell = QStandardItem(date)
        description_cell = QStandardItem(description)
        self.results_model.appendRow([query_cell, date_cell, title_cell, link_cell, description_cell])


def main():
    application = QApplication(sys.argv)
    window = MainWindow()
    desktop = QDesktopWidget().availableGeometry()
    width = (desktop.width() - window.width()) / 2
    height = (desktop.height() - window.height()) / 2
    window.show()
    window.move(width, height)
    sys.exit(application.exec_())


if __name__ == '__main__':
    main()
