from setuptools import setup

requirements = [
    'click',
    'bs4',
    'requests',
    'PyInstaller==3.4',
    'PySide2',
    'peewee==2.10.0',
    'PyQt5'
]

test_requirements = [
    'pytest',
    'pytest-cov',
    'pytest-faulthandler',
    'pytest-mock',
    'pytest-qt',
    'pytest-xvfb',
]

import datseeo

APPNAME = datseeo.__name__
VERSION = datseeo.__version__

setup(
    name=APPNAME,
    version=VERSION,
    description="Simple Google SEO App",
    author="JK",
    author_email='info@bettercallbots.com',
    url='https://gitlab.com/JoshuaKhan_007/datseeo',
    packages=['datseeo', 'datseeo.images'],
    package_data={'datseeo.images': ['*.png']},
    entry_points={
        'console_scripts': [
            'datseeo=datseeo.application:main'
        ]
    },
    install_requires=requirements,
    zip_safe=False,
    keywords='Python3 PyQT5',
    classifiers=[
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
